// Basic setup

const unitLength = 13;
let boxColor = 'rgb(255,204,0)';
const strokeColor = 'hsb(160, 1000, 100%)';
let columns;
let rows;
let currentBoard;
let nextBoard;
let lonelyNum = 2;
let overPopulationNum = 3;
let reproductionNum = 3;
let fr = 60;

// Basic function

function setup() {
    frameRate(fr);
    const canvas = createCanvas(1290, 550);
    canvas.parent(document.querySelector('#canvas'));
    columns = floor(width / unitLength);
    rows = floor(height / unitLength);
    currentBoard = [];
    nextBoard = [];
    for (let i = 0; i < columns; i++) {
        currentBoard[i] = [];
        nextBoard[i] = []
    }
    init();
}

function draw() {
    background(255);
    generate();
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            if (currentBoard[i][j] == 1) {
                fill(color(boxColor));
            } else {
                fill(255);
            }
            stroke(strokeColor);
            rect(i * unitLength, j * unitLength, unitLength, unitLength);
        }
    }
}

function generate() {
    for (let x = 0; x < columns; x++) {
        for (let y = 0; y < rows; y++) {
            let neighbors = 0;
            for (let i of [-1, 0, 1]) {
                for (let j of [-1, 0, 1]) {
                    if (i == 0 && j == 0) {
                        continue;
                    }
                    neighbors += currentBoard[(x + i + columns) % columns][(y + j + rows) % rows];
                }
            }
            if (currentBoard[x][y] == 1 && neighbors < lonelyNum) {
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] == 1 && neighbors > overPopulationNum) {
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] == 0 && neighbors == reproductionNum) {
                nextBoard[x][y] = 1;
            } else {
                nextBoard[x][y] = currentBoard[x][y];
            }
        }
    }

    [currentBoard, nextBoard] = [nextBoard, currentBoard];
}

// Mouse function

function mouseDragged() {

    if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
        return;
    }
    const x = Math.floor(mouseX / unitLength);
    const y = Math.floor(mouseY / unitLength);
    currentBoard[x][y] = 1;
    fill(boxColor);
    stroke(strokeColor);
    rect(x * unitLength, y * unitLength, unitLength, unitLength);
}

function mousePressed() {
    noLoop();
    mouseDragged();
}

function mouseReleased() {
    loop();
}


// Button function

// Speed slider

let speedSlider = document.getElementById("speedRange");
let speedOutput = document.getElementById("speed");
speedOutput.innerHTML = speedSlider.value;

speedSlider.oninput = function () {
    speedOutput.innerHTML = speedSlider.value;
    frameRate(parseInt(speedSlider.value));
    console.log('frameRate ' + this.value);
}

// Restart button

const restarts = document.querySelector('.clear');

restarts.addEventListener('click', function (event) {
    init();
    lonelyNum = 2;
    overPopulationNum = 3;
    reproductionNum = 3;
});

function init() {
    lonelyNum = 2;
    overPopulationNum = 3;
    reproductionNum = 3;
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            currentBoard[i][j] = 0;
            nextBoard[i][j] = 0;
        }
    }
}

// Stop button

document.querySelector('.takePhoto').addEventListener('click', function (event) {
    stop();
});

function stop() {
    frameRate(0)
}

// Play button

document.querySelector('.keepOn').addEventListener('click', function (event) {
    play();
});

function play() {
    let speedNum = parseInt(speedSlider.value);
    frameRate(speedNum);
}


// Movement slider

let onfireSlider = document.getElementById("fireRange");
let onfireOutput = document.getElementById("onfire");
onfireOutput.innerHTML = onfireSlider.value;


onfireSlider.oninput = function () {
    onfireOutput.innerHTML = this.value;
    console.log("move" + onfireSlider.value);
}

// Fire button

document.querySelector('.fire').addEventListener('click', function (event) {
    onFire();
});

function timer(t) {
    return new Promise((rec, rej) => {
        setTimeout(rec, t);
    });
}

let bombTimer = null;

async function onFire() {
    let moveColumnNum = parseInt(onfireSlider.value);
    currentBoard[moveColumnNum + 3 + 2][rows - 4] = 1;
    currentBoard[moveColumnNum + 4 + 2][rows - 5] = 1;
    currentBoard[moveColumnNum + 5 + 2][rows - 1] = 1;
    currentBoard[moveColumnNum + 5 + 2][rows - 5] = 1;
    currentBoard[moveColumnNum + 6 + 2][rows - 2] = 1;
    currentBoard[moveColumnNum + 6 + 2][rows - 3] = 1;
    currentBoard[moveColumnNum + 6 + 2][rows - 4] = 1;
    currentBoard[moveColumnNum + 6 + 2][rows - 5] = 1;
    if (bombTimer == null) {
        bombTimer = setTimeout(bomb, 8000)
    }
}

async function bomb() {
    reproductionNum = 1;
    boxColor = 'rgb(255,8,0)'
    await timer(10);
    boxColor = 'rgb(255,204,0)'
    await timer(10);
    boxColor = 'rgb(255,8,0)'
    await timer(10);
    boxColor = 'rgb(250, 142, 0)'
    await timer(10);
    boxColor = 'rgb(255,204,0)'
    await timer(10);
    boxColor = 'rgb(255,8,0)'
    await timer(10);
    boxColor = 'rgb(250, 142, 0)'
    await timer(10);
    boxColor = 'rgb(255,204,0)'
    await timer(10);
    boxColor = 'rgb(255,8,0)'
    await timer(10);
    boxColor = 'rgb(250, 142, 0)'
    await timer(10);
    boxColor = 'rgb(255,204,0)'
    await timer(10);
    boxColor = 'rgb(255,8,0)'
    await timer(10);
    boxColor = 'rgb(250, 142, 0)'
    await timer(10);
    boxColor = 'rgb(255,8,0)'
    await timer(300);
    reproductionNum = 8;
    await timer(20);
    overPopulationNum = 0;
    await timer(200);
    boxColor = 'rgb(255,204,0)'
    overPopulationNum = 3;
    reproductionNum = 3;
    bombTimer = null;
}

// Buttons (not display "lonely, over population, reproduction")

// Lonely

let lonelySlider = document.getElementById("lonelyRange");
let lonelyOutput = document.getElementById("lonely");
lonelyOutput.innerHTML = lonelySlider.value;

lonelySlider.oninput = function () {
    lonelyOutput.innerHTML = this.value;
    changeLonelyRule(this.value);
    console.log("Lonely" + lonelyNum);
}

function changeLonelyRule(i) {
    lonelyNum = i;
}

// Over population

let overPopulationSlider = document.getElementById("overPopulationRange");
let overPopulationOutput = document.getElementById("overPopulation");
overPopulationOutput.innerHTML = overPopulationSlider.value;


overPopulationSlider.oninput = function () {
    overPopulationOutput.innerHTML = this.value;
    changeOverPopulationRule(this.value);
    console.log("Over Population" + overPopulationNum);
}

function changeOverPopulationRule(i) {
    overPopulationNum = i;
}

// Reproduction

let reproductionSlider = document.getElementById("reproductionRange");
let reproductionOutput = document.getElementById("reproduction");
reproductionOutput.innerHTML = reproductionSlider.value;


reproductionSlider.oninput = function () {
    reproductionOutput.innerHTML = this.value;
    changeReproductionRule(this.value);
    console.log("Reproduction" + reproductionNum);
}

function changeReproductionRule(i) {
    reproductionNum = i;
}

// button1

document.querySelector('.button1').addEventListener('click', function (event) {
    button1();
});

function button1() {
    currentBoard[1][3] = 1;
    currentBoard[2][3] = 1;
    currentBoard[3][3] = 1;
    currentBoard[3][2] = 1;
    currentBoard[2][1] = 1;
}
