// Basic setup

let unitLength  = 10;
let boxColor = 'rgb(30,104,149)';
const strokeColor = 225;
let columns;
let rows; 
let currentBoard;
let nextBoard;
let fr = 0;

// Basic function

function setup(){
    frameRate(fr);
    const canvas = createCanvas(1295, window.innerHeight - 190);
    canvas.parent(document.querySelector('#canvas'));
    columns = floor(width  / unitLength);
    rows    = floor(height / unitLength);
    currentBoard = [];
    nextBoard = [];
    for (let i = 0; i < columns; i++) {
        currentBoard[i] = [];
        nextBoard[i] = []
    }
    init(); 
}

function draw() {
    background(255);
    generate();
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            if (currentBoard[i][j] == 1){
                fill(color(boxColor));  
            } else {
                fill(255);
            } 
            stroke(strokeColor);
            rect(i * unitLength, j * unitLength, unitLength, unitLength, 20);
        }
    }
}

function generate() {
    for (let x = 0; x < columns; x++) {
        for (let y = 0; y < rows; y++) {
            let neighbors = 0;
            for (let i of [-1, 0, 1]) {
                for (let j of [-1, 0, 1]) {
                    if( i == 0 && j == 0 ){
                        continue;
                    }
                    neighbors += currentBoard[(x + i + columns) % columns][(y + j + rows) % rows];
                }
            }
            if (currentBoard[x][y] == 1 && neighbors < 8) {
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] == 1 && neighbors > 0) {
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] == 0 && neighbors == 0) {
                nextBoard[x][y] = 1;
            } else {
                nextBoard[x][y] = currentBoard[x][y];
            }
        }
    }
    [currentBoard, nextBoard] = [nextBoard, currentBoard];
}

// Mouse function

function mouseDragged() {
    if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
        return;
    }
    const x = Math.floor(mouseX / unitLength);
    const y = Math.floor(mouseY / unitLength);
    currentBoard[x][y] = 1;
    fill(boxColor);
    stroke(strokeColor);
    rect(x * unitLength, y * unitLength, unitLength, unitLength);
}

function mouseReleased() {
    loop();
}

// Button function

// Speed slider

let speedSlider = document.getElementById("speedRange");
let speedOutput = document.getElementById("speed");
speedOutput.innerHTML = speedSlider.value; 

speedSlider.oninput = function() {
    speedOutput.innerHTML = speedSlider.value;
    frameRate(parseInt(speedSlider.value));
    console.log('frameRate ' + this.value);
}

// Restart button

const restarts = document.querySelector('.refashPopUp');

restarts.addEventListener('click', function(event){
        init();
        });

function  init() {
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            currentBoard[i][j] = 0;
            nextBoard[i][j] = 0;
        }
    }
}

// Stop button

document.querySelector('.stopPopUp').addEventListener('click', function(event){
    stop();
});

function  stop() {
    frameRate(0)
}

// Play button

document.querySelector('.playPopUp').addEventListener('click', function(event){
    play();
});

function  play() {
    frameRate(2);
}

// Color button

// Red

document.querySelector('.redPopUp').addEventListener('click', function(event){
    boxColor = 'rgb(138,21,0)';
});

// Orange

document.querySelector('.orangePopUp').addEventListener('click', function(event){
    boxColor = 'rgb(211,100,35)';
});

// Yellow color

document.querySelector('.yellowPopUp').addEventListener('click', function(event){
    boxColor = 'rgb(248,211,0)';
});

// Green color

document.querySelector('.greenPopUp').addEventListener('click', function(event){
    boxColor = 'rgb(0,212,11)';
});

// Aque color

document.querySelector('.aquePopUp').addEventListener('click', function(event){
    boxColor = 'rgb(11,227,255)';
});

// Blue color

document.querySelector('.bluePopUp').addEventListener('click', function(event){
    boxColor = 'rgb(30,104,149)';
});

// Purple color

document.querySelector('.purplePopUp').addEventListener('click', function(event){
    boxColor = 'rgb(212,0,219)';
});


