// Basic setup

const unitLength = 10;
let boxColor = 'rgb(102,61,22)';
const strokeColor = 225;
let columns;
let rows;
let currentBoard;
let nextBoard;
let boardColor;
let lonelyNum = 2;
let overPopulationNum = 3;
let reproductionNum = 3;
let fr = 15;

// Basic function

function setup() {
    frameRate(fr);
    const canvas = createCanvas(1200, window.innerHeight - 310);
    canvas.parent(document.querySelector('#canvas'));
    columns = floor(width / unitLength);
    rows = floor(height / unitLength);
    currentBoard = [];
    nextBoard = [];
    boardColor = [];
    for (let i = 0; i < columns; i++) {
        currentBoard[i] = [];
        nextBoard[i] = [];
        boardColor[i] = [];
    }
    init();
}

function draw() {
    background(255);
    generate();
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            if (currentBoard[i][j] == 1) {
                fill(color(boardColor[i][j]));
            } else {
                fill(255);
            }
            stroke(strokeColor);
            rect(i * unitLength, j * unitLength, unitLength, unitLength);
        }
    }
}

function generate() {
    for (let x = 0; x < columns; x++) {
        for (let y = 0; y < rows; y++) {
            let neighbors = 0;
            let nextColor = 0
            for (let i of [-1, 0, 1]) {
                for (let j of [-1, 0, 1]) {
                    if (i == 0 && j == 0) {
                        continue;
                    }
                    let isAlive = currentBoard[(x + i + columns) % columns][(y + j + rows) % rows];
                    if (isAlive) {
                        nextColor = boardColor[(x + i + columns) % columns][(y + j + rows) % rows];
                    }
                    neighbors += isAlive;
                }
            }
            if (currentBoard[x][y] == 1 && neighbors < lonelyNum) {
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] == 1 && neighbors > overPopulationNum) {
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] == 0 && neighbors == reproductionNum) {
                nextBoard[x][y] = 1;
                boardColor[x][y] = nextColor
            } else {
                nextBoard[x][y] = currentBoard[x][y];
            }
        }
    }
    [currentBoard, nextBoard] = [nextBoard, currentBoard];
}

// Mouse function

function mouseDragged() {
    if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
        return;
    }
    const x = Math.floor(mouseX / unitLength);
    const y = Math.floor(mouseY / unitLength);
    currentBoard[x][y] = 1;
    boardColor[x][y] = boxColor;
    fill(boxColor);
    stroke(strokeColor);
    rect(x * unitLength, y * unitLength, unitLength, unitLength);
}

function mousePressed() {
    noLoop();
    mouseDragged();
}

function mouseReleased() {
    loop();
}


// Slider function

// Lonely slider

let lonelySlider = document.getElementById("lonelyRange");
let lonelyOutput = document.getElementById("lonely");
lonelyOutput.innerHTML = lonelySlider.value;


lonelySlider.oninput = function () {
    lonelyOutput.innerHTML = this.value;
    changeLonelyRule(this.value);
    console.log("Lonely" + lonelyNum);
}

function changeLonelyRule(i) {
    lonelyNum = i;
}

// Over Population slider

let overPopulationSlider = document.getElementById("overPopulationRange");
let overPopulationOutput = document.getElementById("overPopulation");
overPopulationOutput.innerHTML = overPopulationSlider.value;


overPopulationSlider.oninput = function () {
    overPopulationOutput.innerHTML = this.value;
    changeOverPopulationRule(this.value);
    console.log("Over Population" + overPopulationNum);
}

function changeOverPopulationRule(i) {
    overPopulationNum = i;
}


// Reproduction slider

let reproductionSlider = document.getElementById("reproductionRange");
let reproductionOutput = document.getElementById("reproduction");
reproductionOutput.innerHTML = reproductionSlider.value;


reproductionSlider.oninput = function () {
    reproductionOutput.innerHTML = this.value;
    changeReproductionRule(this.value);
    console.log("Reproduction" + reproductionNum);
}

function changeReproductionRule(i) {
    reproductionNum = i;
}

// Speed slider

let speedSlider = document.getElementById("speedRange");
let speedOutput = document.getElementById("speed");
speedOutput.innerHTML = speedSlider.value;

speedSlider.oninput = function () {
    speedOutput.innerHTML = speedSlider.value;
    frameRate(parseInt(speedSlider.value));
    console.log('frameRate ' + this.value);
}


// Button function

// Restart button

const restarts = document.querySelector('.restartButton');

restarts.addEventListener('click', function (event) {
    init();
    boxColor = 'rgb(102,61,22)';
});

function init() {
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            currentBoard[i][j] = 0;
            nextBoard[i][j] = 0;
        }
    }
}


// Stop button

document.querySelector('.stop').addEventListener('click', function (event) {
    stop();
});

function stop() {
    frameRate(0)
}

// Play button

document.querySelector('.play').addEventListener('click', function (event) {
    play();
});

function play() {
    frameRate(15);
}

// Glider

// Glider left

document.querySelector('.gliderLeft').addEventListener('click', function (event) {
    gliderLeftButton();
});

function gliderLeftButton() {
    currentBoard[1][3] = 1;
    currentBoard[2][3] = 1;
    currentBoard[3][3] = 1;
    currentBoard[3][2] = 1;
    currentBoard[2][1] = 1;

    currentBoard[1 + 10][3] = 1;
    currentBoard[2 + 10][3] = 1;
    currentBoard[3 + 10][3] = 1;
    currentBoard[3 + 10][2] = 1;
    currentBoard[2 + 10][1] = 1;

    currentBoard[2 + 5][3] = 1;
    currentBoard[1 + 5][3] = 1;
    currentBoard[3 + 5][3] = 1;
    currentBoard[3 + 5][2] = 1;
    currentBoard[2 + 5][1] = 1;
    
    boardColor[1][3] = 'rgb(255,196,0)';
    boardColor[2][3] = 'rgb(255,196,0)';
    boardColor[3][3] = 'rgb(255,196,0)';
    boardColor[3][2] = 'rgb(255,196,0)';
    boardColor[2][1] = 'rgb(255,196,0)';

    boardColor[1 + 10][3] = 'rgb(255,196,0)';
    boardColor[2 + 10][3] = 'rgb(255,196,0)';
    boardColor[3 + 10][3] = 'rgb(255,196,0)';
    boardColor[3 + 10][2] = 'rgb(255,196,0)';
    boardColor[2 + 10][1] = 'rgb(255,196,0)';

    boardColor[2 + 5][3] = 'rgb(255,196,0)';
    boardColor[1 + 5][3] = 'rgb(255,196,0)';
    boardColor[3 + 5][3] = 'rgb(255,196,0)';
    boardColor[3 + 5][2] = 'rgb(255,196,0)';
    boardColor[2 + 5][1] = 'rgb(255,196,0)';
}

// Glider middle

document.querySelector('.gliderMiddle').addEventListener('click', function (event) {
    gliderMiddleButton();
});

function gliderMiddleButton() {
    currentBoard[1 + 43][3] = 1;
    currentBoard[2 + 43][3] = 1;
    currentBoard[3 + 43][3] = 1;
    currentBoard[3 + 43][2] = 1;
    currentBoard[2 + 43][1] = 1;

    currentBoard[1 + 10 + 43][3] = 1;
    currentBoard[2 + 10 + 43][3] = 1;
    currentBoard[3 + 10 + 43][3] = 1;
    currentBoard[3 + 10 + 43][2] = 1;
    currentBoard[2 + 10 + 43][1] = 1;

    currentBoard[2 + 5 + 43][3] = 1;
    currentBoard[1 + 5 + 43][3] = 1;
    currentBoard[3 + 5 + 43][3] = 1;
    currentBoard[3 + 5 + 43][2] = 1;
    currentBoard[2 + 5 + 43][1] = 1;
    
    boardColor[1 + 43][3] = 'rgb(255,196,0)';
    boardColor[2 + 43][3] = 'rgb(255,196,0)';
    boardColor[3 + 43][3] = 'rgb(255,196,0)';
    boardColor[3 + 43][2] = 'rgb(255,196,0)';
    boardColor[2 + 43][1] = 'rgb(255,196,0)';

    boardColor[1 + 10 + 43][3] = 'rgb(255,196,0)';
    boardColor[2 + 10 + 43][3] = 'rgb(255,196,0)';
    boardColor[3 + 10 + 43][3] = 'rgb(255,196,0)';
    boardColor[3 + 10 + 43][2] = 'rgb(255,196,0)';
    boardColor[2 + 10 + 43][1] = 'rgb(255,196,0)';

    boardColor[2 + 5 + 43][3] = 'rgb(255,196,0)';
    boardColor[1 + 5 + 43][3] = 'rgb(255,196,0)';
    boardColor[3 + 5 + 43][3] = 'rgb(255,196,0)';
    boardColor[3 + 5 + 43][2] = 'rgb(255,196,0)';
    boardColor[2 + 5 + 43][1] = 'rgb(255,196,0)';
}

// Glider Right

document.querySelector('.gliderRight').addEventListener('click', function (event) {
    gliderRightButton();
});

function gliderRightButton() {
    currentBoard[1 + 43 + 43][3] = 1;
    currentBoard[2 + 43 + 43][3] = 1;
    currentBoard[3 + 43 + 43][3] = 1;
    currentBoard[3 + 43 + 43][2] = 1;
    currentBoard[2 + 43 + 43][1] = 1;

    currentBoard[1 + 10 + 43 + 43][3] = 1;
    currentBoard[2 + 10 + 43 + 43][3] = 1;
    currentBoard[3 + 10 + 43 + 43][3] = 1;
    currentBoard[3 + 10 + 43 + 43][2] = 1;
    currentBoard[2 + 10 + 43 + 43][1] = 1;

    currentBoard[2 + 5 + 43 + 43][3] = 1;
    currentBoard[1 + 5 + 43 + 43][3] = 1;
    currentBoard[3 + 5 + 43 + 43][3] = 1;
    currentBoard[3 + 5 + 43 + 43][2] = 1;
    currentBoard[2 + 5 + 43 + 43][1] = 1;
    
    boardColor[1 + 43 + 43][3] = 'rgb(255,196,0)';
    boardColor[2 + 43 + 43][3] = 'rgb(255,196,0)';
    boardColor[3 + 43 + 43][3] = 'rgb(255,196,0)';
    boardColor[3 + 43 + 43][2] = 'rgb(255,196,0)';
    boardColor[2 + 43 + 43][1] = 'rgb(255,196,0)';

    boardColor[1 + 10 + 43 + 43][3] = 'rgb(255,196,0)';
    boardColor[2 + 10 + 43 + 43][3] = 'rgb(255,196,0)';
    boardColor[3 + 10 + 43 + 43][3] = 'rgb(255,196,0)';
    boardColor[3 + 10 + 43 + 43][2] = 'rgb(255,196,0)';
    boardColor[2 + 10 + 43 + 43][1] = 'rgb(255,196,0)';

    boardColor[2 + 5 + 43 + 43][3] = 'rgb(255,196,0)';
    boardColor[1 + 5 + 43 + 43][3] = 'rgb(255,196,0)';
    boardColor[3 + 5 + 43 + 43][3] = 'rgb(255,196,0)';
    boardColor[3 + 5 + 43 + 43][2] = 'rgb(255,196,0)';
    boardColor[2 + 5 + 43 + 43][1] = 'rgb(255,196,0)';
}

// Spaceship

// Spaceship top

document.querySelector('.button2').addEventListener('click', function (event) {
    button2();
});

function button2() {
    currentBoard[1][3] = 1;
    currentBoard[4][3] = 1;
    currentBoard[5][4] = 1;
    currentBoard[1][5] = 1;
    currentBoard[5][5] = 1;
    currentBoard[2][6] = 1;
    currentBoard[3][6] = 1;
    currentBoard[4][6] = 1;
    currentBoard[5][6] = 1;

    boardColor[1][3] = 'rgb(199,56,0)';
    boardColor[4][3] = 'rgb(199,56,0)';
    boardColor[5][4] = 'rgb(199,56,0)';
    boardColor[1][5] = 'rgb(199,56,0)';
    boardColor[5][5] = 'rgb(199,56,0)';
    boardColor[2][6] = 'rgb(199,56,0)';
    boardColor[3][6] = 'rgb(199,56,0)';
    boardColor[4][6] = 'rgb(199,56,0)';
    boardColor[5][6] = 'rgb(199,56,0)';
}

// Spaceship middle

document.querySelector('.spaceshipMiddle').addEventListener('click', function (event) {
    spaceshipMiddleButton();
});

function spaceshipMiddleButton() {
    currentBoard[1][3 + 16] = 1;
    currentBoard[4][3 + 16] = 1;
    currentBoard[5][4 + 16] = 1;
    currentBoard[1][5 + 16] = 1;
    currentBoard[5][5 + 16] = 1;
    currentBoard[2][6 + 16] = 1;
    currentBoard[3][6 + 16] = 1;
    currentBoard[4][6 + 16] = 1;
    currentBoard[5][6 + 16] = 1;

    boardColor[1][3 + 16] = 'rgb(199,56,0)';
    boardColor[4][3 + 16] = 'rgb(199,56,0)';
    boardColor[5][4 + 16] = 'rgb(199,56,0)';
    boardColor[1][5 + 16] = 'rgb(199,56,0)';
    boardColor[5][5 + 16] = 'rgb(199,56,0)';
    boardColor[2][6 + 16] = 'rgb(199,56,0)';
    boardColor[3][6 + 16] = 'rgb(199,56,0)';
    boardColor[4][6 + 16] = 'rgb(199,56,0)';
    boardColor[5][6 + 16] = 'rgb(199,56,0)';
}

// Spaceship low

document.querySelector('.spaceshipLow').addEventListener('click', function (event) {
    spaceshipLowButton();
});

function spaceshipLowButton() {
    currentBoard[1][3 + 34] = 1;
    currentBoard[4][3 + 34] = 1;
    currentBoard[5][4 + 34] = 1;
    currentBoard[1][5 + 34] = 1;
    currentBoard[5][5 + 34] = 1;
    currentBoard[2][6 + 34] = 1;
    currentBoard[3][6 + 34] = 1;
    currentBoard[4][6 + 34] = 1;
    currentBoard[5][6 + 34] = 1;

    boardColor[1][3 + 34] = 'rgb(199,56,0)';
    boardColor[4][3 + 34] = 'rgb(199,56,0)';
    boardColor[5][4 + 34] = 'rgb(199,56,0)';
    boardColor[1][5 + 34] = 'rgb(199,56,0)';
    boardColor[5][5 + 34] = 'rgb(199,56,0)';
    boardColor[2][6 + 34] = 'rgb(199,56,0)';
    boardColor[3][6 + 34] = 'rgb(199,56,0)';
    boardColor[4][6 + 34] = 'rgb(199,56,0)';
    boardColor[5][6 + 34] = 'rgb(199,56,0)';
}

