// Basic setup

const unitLength = 10;
const boxColor = 'rgb(1,151,139)';
const strokeColor = 225;
let columns;
let rows;
let currentBoard;
let nextBoard;

// Basic function

function setup() {
    const canvas = createCanvas(500, 500);
    canvas.parent(document.querySelector('#canvas'));
    columns = floor(width / unitLength);
    rows = floor(height / unitLength);
    currentBoard = [];
    nextBoard = [];
    for (let i = 0; i < columns; i++) {
        currentBoard[i] = [];
        nextBoard[i] = []
    }
    init();
}

function draw() {
    background(255);
    generate();
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            if (currentBoard[i][j] == 1) {
                fill(boxColor);
            } else {
                fill(255);
            }
            stroke(strokeColor);
            rect(i * unitLength, j * unitLength, unitLength, unitLength, 20);
        }
    }
}

function generate() {
    for (let x = 0; x < columns; x++) {
        for (let y = 0; y < rows; y++) {
            let neighbors = 0;
            for (let i of [-1, 0, 1]) {
                for (let j of [-1, 0, 1]) {
                    if (i === 0 && j === 0) {
                        continue;
                    }
                    neighbors += currentBoard[(x + i + columns) % columns][(y + j + rows) % rows];
                }
            }
            if (currentBoard[x][y] == 1 && neighbors < 2) {
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] == 1 && neighbors > 3) {
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] == 0 && neighbors == 3) {
                nextBoard[x][y] = 1;
            } else {
                nextBoard[x][y] = currentBoard[x][y];
            }
        }
    }
    [currentBoard, nextBoard] = [nextBoard, currentBoard];
}


// Mouse function

function mouseDragged() {
    if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
        return;
    }
    const x = Math.floor(mouseX / unitLength);
    const y = Math.floor(mouseY / unitLength);
    currentBoard[x][y] = 1;
    fill(boxColor);
    stroke(strokeColor);
    rect(x * unitLength, y * unitLength, unitLength, unitLength);
}

function mousePressed() {
    noLoop();
    mouseDragged();
}

function mouseReleased() {
    loop();
}


// Button function

// Restart button

const resetGameButton = document.querySelector('#resetGame');
    resetGameButton.addEventListener('click', function (event) {
        init();
    });


function init() {
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            currentBoard[i][j] = 0;
            nextBoard[i][j] = 0;
        }
    }
}

// No Mask button

function noMaskGameButton() {
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            currentBoard[i][j] = floor(random(2));
            nextBoard[i][j] = 0;
        }
    }
}

const noMaskButton = document.querySelector('#noMask');
    noMaskButton.addEventListener('click', function (event) {
        noMaskGameButton();
    });

// infectedCount function

function infectedCount(infectedFactor) {
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            let infectedRate = [1];
            for (let q = 0; q < infectedFactor; q++) {
                infectedRate.push(0)
            }
            currentBoard[i][j] = floor(random(infectedRate));
            nextBoard[i][j] = 0;
        }
    }
}

// Normal Mask button

const normalMaskButton = document.querySelector('#normalMask');
    normalMaskButton.addEventListener('click', function (event) {
        infectedCount(7);
    });


// N95 Mask button

const n95Button = document.querySelector('#n95');
    n95Button.addEventListener('click', function (event) {
        infectedCount(12);
    });

